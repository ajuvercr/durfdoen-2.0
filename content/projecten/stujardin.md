---
titel: StuJardin
id: stujardin
naam: StuJardin
verkorte_naam: StuJardin
konvent: gsr
logo: /assets/sfeerfotos/stujardin.png
contact: http://stujardin@gmail.com/
website: http://student.ugent.be/stujardin/
social:
  - platform: facebook
    link: https://www.facebook.com/stujardin/
themas:
  -  duurzaamheid en groen
---

StuJardin teelt samen met studenten groenten, fruit en kruiden in de stad. Hiermee willen we studenten overtuigen dat stadstuinieren plezierig en noodzakelijk is. Het project is een stap naar een stedelijke samenleving die meer berust op zelfvoorziening en korte keten.
Het is immers duurzamer en milieuvriendelijker om je eigen lokaal geteelde groenten te consumeren dan voedsel te kopen dat lange afstanden heeft afgelegd voor het op je bord terecht komt.
Verder proberen we de consument bewust te maken van het productieproces door er in de praktijk mee bezig te zijn.
Concreet bestaat het project uit twee luiken. Enerzijds hebben we een permanente moestuin waarin we werken op vaste werkmomenten, maar waar iedereen steeds welkom is.
Anderzijds worden er in het tweede semester diverse groeiavonden georganiseerd. Dit jaar komt er naast een tweede editie van tuinieren op kot, ook een workshop rond composteren, een sprokkelwandeling rond insectenhotels en een kruidenwandeling.
Het project ontstond als een samenwerking tussen JNM (Jeugdbond voor Natuur en Milieu) en d'Urgent (de studentenvereniging voor een duurzame universiteit, toen nog UGent1010 genoemd). Later werd de GSR (Gentse Studentenraad) de derde partner in het project. StuJardin heeft ook steun gekregen van Kruiden Claus en Stad Gent. 
