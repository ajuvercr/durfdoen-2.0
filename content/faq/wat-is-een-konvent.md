---
vraag: 'Wat is een konvent?'
---

Verenigingen zijn gegroepeerd in konventen. Deze konventen ondersteunen hen op allerlei manieren, zoals het organiseren van sporttornooien, het verdelen van subsidies en hen advies verlenen over hun werking. Er zijn zeven konventen aan de UGent: het [Seniorenkonvent](https://skghendt.be/index/), het [Faculteitenkonvent](https://fkgent.be/), het [Homekonvent](http://www.homekonvent.be/), het [Werkgroepen- en Verenigingenkonvent](https://wvk.ugent.be/), het [Politiek-Filosofisch konvent](https://pfk.ugent.be/), het [Internationaal konvent](https://internationaalkonv.wixsite.com/internationalkonvent) en het [Kultureel Konvent](http://www.student.ugent.be/kultkcentraal/).
