---
vraag: 'Mijn vereniging staat er niet tussen/De informatie over mijn vereniging is incorrect. Wat kan ik hier aan doen?'
---
Iedereen kan de informatie voor zijn eigen vereniging aanpassen met een pull request, [je vindt hier de instructies om dat heel eenvoudig te doen](https://github.ugent.be/ZeusWPI/durfdoen-2.0). Als je computers eng vindt mag je ons ook altijd [een mailtje sturen](mailto:durfdoen@gentsestudentenraad.be).
