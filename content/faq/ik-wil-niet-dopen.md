---
vraag: Ik wil graag bij een kring of club, maar wil me niet laten dopen, moet dat?
---
Hoewel een doop helemaal niet zo eng is, snappen we helemaal dat niet iedereen dit wil doen. Je hoeft dan ook niet bang te zijn, naar het grootste deel van de activiteiten kan je gewoon zonder gedoopt te zijn! Enkel sommige cantussen van sommige verenigingen zijn gesloten, maar vraag dit gerust eens na bij mensen van de verenigingen zelf.
