---
vraag: Wat is durf doen?
---
Durf Doen is een project van de [Gentse Studentenraad](https://gentsestudentenraad.be), de Vergadering der Konventsvoorzitters en [Zeus WPI](https://zeus.gent). Het doel is alle studentenverenigingen die erkend zijn aan de UGent bekend te maken bij de studenten. Zo kan iedere student een vrije tijdsbesteding vinden die bij hen past. Want de UGent is meer dan studeren alleen.
