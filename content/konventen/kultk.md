---
naam: Kultureel Konvent
id: kultk
verkorte_naam: Kultureel Konvent
contact: kultureelkonvent@gmail.com
website: http://student.ugent.be/kultkcentraal/index.html
social:
  - platform: facebook
    link: https://www.facebook.com/KultureelKonvent/
themas:
  - cultuur
---

Het KultK telt acht verenigingen die elk een eigen deel van de Kultuur met de grote K proberen te introduceren aan de medestudenten.
