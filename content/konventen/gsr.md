---
naam: Gentse Studentenraad
id: gsr
verkorte_naam: GSR
contact: info@gentsestudentenraad.be
website: https://www.gentsestudentenraad.be
social:
  - platform: facebook
    link: https://www.facebook.com/gentsestudentenraad
  - platform: twitter
    link: https://twitter.com/GentseStud
  - platform: instagram
    link: https://www.instagram.com/gentsestudentenraad/
themas:
  - engagement
showcase:
  - photo: /assets/logos/GSRA.jpg
  - photo: /assets/logos/GSRB.jpg
  - photo: /assets/logos/GSRC.jpg
---

De Gentse Studentenraad is de centrale studentenraad van de UGent. De Gentse Studentenraad vertegenwoordigt en verdedigt (de belangen van) de studenten bij het universiteitsbestuur. Gaande van onderwijsaangelegenheden (Onderwijs- en examenreglement (OER), bijzondere statuten, studiemaatregelen ...) tot sociale zaken (homes, studentenresto's), de thema's die binnen de Gentse Studentenraad worden aangesneden zijn bijzonder divers.
De Gentse Studentenraad is echter op meer niveaus aanwezig dan enkel het universitaire. In de eerste plaats ondersteunt de Gentse Studentenraad de elf facultaire studentenraden. Ook op het stedelijk niveau en op het Vlaams niveau is de Gentse Studentenraad actief. Daarenboven ondersteunt de Gentse Studentenraad ook verschillende grote en minder grote projecten zoals de Fietsambassade, Cultour en Start to Talk.
