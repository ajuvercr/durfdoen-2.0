---
vraag: Wat is je faculteit?
type: dropdown
antwoorden:
  - tekst: Letteren en Wijsbegeerte
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: Afrikaanse talen en culturen
        verenigingen:
          - naam: stuart
          - naam: oak

      - tekst: Archeologie
        verenigingen:
          - naam: stuart
          - naam: aw
          - naam: khk

      - tekst: Geschiedenis
        verenigingen:
          - naam: stuart
          - naam: vgeschiedk
          - naam: isha

      - tekst: Kunstwetenschappen
        verenigingen:
          - naam: stuart
          - naam: khk

      - tekst: Moraalwetenschappen
        verenigingen:
          - naam: stuart
          - naam: kmf

      - tekst: Oosterse talen en culturen
        verenigingen:
          - naam: stuart
          - naam: oak
          - naam: tnk

      - tekst: Oost- Europese talen en culturen
        verenigingen:
          - naam: stuart
          - naam: slavia

      - tekst: Taal- en letterkunde
        verenigingen:
          - naam: stuart
          - naam: kk
          - naam: filologica

      - tekst: Toegepaste taalkunde
        verenigingen:
          - naam: stuart
          - naam: veto

      - tekst: Wijsbegeerte
        verenigingen:
          - naam: stuart
          - naam: kmf

  - tekst: Recht en criminologie
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: Rechten
        verenigingen:
          - naam: fre
          - naam: vrg

      - tekst: Criminologische wetenschappen
        verenigingen:
          - naam: fre
          - naam: lombrosiana

  - tekst: Wetenschappen
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: Biochemie en biotechnologie
        verenigingen:
          - naam: stuw
          - naam: chemica

      - tekst: Biologie
        verenigingen:
          - naam: stuw
          - naam: gbk

      - tekst: Chemie
        verenigingen:
          - naam: stuw
          - naam: chemica
      - tekst: Fysica en de sterrenkunde

        verenigingen:
          - naam: stuw
          - naam: vvn
          - naam: wina

      - tekst: Geografie en de geomatica
        verenigingen:
          - naam: stuw
          - naam: geografica

      - tekst: Geologie
        verenigingen:
          - naam: stuw
          - naam: geologica

      - tekst: Informatica
        verenigingen:
          - naam: stuw
          - naam: zeus
          - naam: wina

      - tekst: Wiskunde
        verenigingen:
          - naam: stuw
          - naam: prime
          - naam: wina

  - tekst: Geneeskunde en gezondheidswetenschappen
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: Biomedische wetenschappen
        verenigingen:
          - naam: stugg
          - naam: beam
          - naam: bemsa
          - naam: vbk
          - naam: stuggbsr

      - tekst: Geneeskunde
        verenigingen:
          - naam: stugg
          - naam: vgeneesk
          - naam: bemsa

      - tekst: Lichamelijke opvoeding en de bewegingswetenschappen
        verenigingen:
          - naam: stugg
          - naam: hilok
          - naam: bemsa

      - tekst: Audiologie
        verenigingen:
          - naam: stugg
          - naam: vlak
          - naam: bemsa

      - tekst: Logopedie
        verenigingen:
          - naam: stugg
          - naam: vlak
          - naam: bemsa

      - tekst: Revalidatiewetenschappen en de kinesitherapie
        verenigingen:
          - naam: stugg
          - naam: hilok
          - naam: bemsa

      - tekst: Tandheelkunde
        verenigingen:
          - naam: stugg
          - naam: dentalia
          - naam: bemsa

  - tekst: Ingenieurswetenschappen en architectuur
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: iw - Bouwkunde
        verenigingen:
          - naam: fris
          - naam: hermes

      - tekst: iw - Chemie
        verenigingen:
          - naam: fris
          - naam: hermes

      - tekst: iw - Elektronica-ICT
        verenigingen:
          - naam: fris
          - naam: zeus
          - naam: hermes

      - tekst: iw - Elektromechanica
        verenigingen:
          - naam: fris
          - naam: hermes

      - tekst: iw - Informatica
        verenigingen:
          - naam: fris
          - naam: zeus
          - naam: hermes

      - tekst: ir - Architectuur
        verenigingen:
          - naam: fris
          - naam: dlk
          - naam: vtk

      - tekst: ir - Bouwkunde
        verenigingen:
          - naam: fris
          - naam: poutrix
          - naam: vtk

      - tekst: ir - Chemische technologie en materiaalkunde
        verenigingen:
          - naam: fris
          - naam: macht
          - naam: vtk

      - tekst: ir - Computerwetenschappen
        verenigingen:
          - naam: fris
          - naam: ceneka
          - naam: ieee
          - naam: zeus
          - naam: vtk

      - tekst: ir - Elektrotechniek
        verenigingen:
          - naam: fris
          - naam: ceneka
          - naam: ieee
          - naam: vtk

      - tekst: ir - Toegepaste natuurkunde
        verenigingen:
          - naam: fris
          - naam: vtk

      - tekst: ir - Werktuigkunde-elektrotechniek
        verenigingen:
          - naam: fris
          - naam: ceneka
          - naam: ieee
          - naam: pkarus
          - naam: vtk

      - tekst: ir - Biomedische ingenieurstechnieken
        verenigingen:
          - naam: fris
          - naam: beam
          - naam: vtk

      - tekst: iw - Industrieel ontwerpen
        verenigingen:
          - naam: fris
          - naam: hermes

      - tekst: iw - Machine- en productieautomatisering
        verenigingen:
          - naam: fris
          - naam: hermes

  - tekst: Economie en bedrijfskude
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: Handelsingenieur
        verenigingen:
          - naam: stuveco
          - naam: capitant
          - naam: flyse
          - naam: vek

      - tekst: Bestuurskunde en het publiek management
        verenigingen:
          - naam: stuveco
          - naam: lies

      - tekst: Economische wetenschappen
        verenigingen:
          - naam: stuveco
          - naam: capitant
          - naam: flyse
          - naam: vek

      - tekst: Handelswetenschappen
        verenigingen:
          - naam: stuveco
          - naam: capitant
          - naam: flyse
          - naam: lies

      - tekst: Toegepaste economische wetenschappen
        verenigingen:
          - naam: stuveco
          - naam: capitant
          - naam: flyse
          - naam: vek

  - tekst: Diergeneeskunde
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: Diergeneeskunde
        verenigingen:
          - naam: fde
          - naam: vdk

  - tekst: Psychologie en pedagogische wetenschappen
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: Pedagogie - Klinische orthopedagogiek en disability studies
        verenigingen:
          - naam: ppsr
          - naam: vppk

      - tekst: Pedagogie - Pedagogiek en onderwijskunde
        verenigingen:
          - naam: ppsr
          - naam: vppk

      - tekst: Pedagogie - Sociale agogiek
        verenigingen:
          - naam: ppsr
          - naam: vppk

      - tekst: Psychologie - Bedrijfspsychologie en personeelsbeleid
        verenigingen:
          - naam: ppsr
          - naam: vppk

      - tekst: Psychologie - Klinische psychologie
        verenigingen:
          - naam: ppsr
          - naam: vppk

      - tekst: Psychologie - onderwijs
        verenigingen:
          - naam: ppsr
          - naam: vppk

      - tekst: Psychologie - Theoretische en experimentele psychologie
        verenigingen:
          - naam: ppsr
          - naam: vppk

  - tekst: Bio-ingenieurswetenschappen
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: biw - Bos- en natuurbeheer
        verenigingen:
          - naam: stubio
          - naam: vlk

      - tekst: biw - cel- en genbiotechnologie
        verenigingen:
          - naam: stubio
          - naam: vlk

      - tekst: biw - Chemie en voedingstechnologie
        verenigingen:
          - naam: stubio
          - naam: vlk

      - tekst: biw - Landbouwkunde
        verenigingen:
          - naam: stubio
          - naam: vlk
          - naam: gk
          - naam: iaas

      - tekst: biw - Land- en waterbeheer
        verenigingen:
          - naam: stubio
          - naam: vlk

      - tekst: biw - Milieutechnologie
        verenigingen:
          - naam: stubio
          - naam: vlk

      - tekst: Biowetenschappen
        verenigingen:
          - naam: stubio
          - naam: lila

  - tekst: Farmaceutische wetenschappen
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: Farmaceutische wetenschappen
        verenigingen:
          - naam: stuff
          - naam: gfk
          - naam: bemsa

  - tekst: Politieke en sociale wetenschappen
    vraag: Welke richting?
    type: dropdown
    antwoorden:
      - tekst: Communicatiewetenschappen
        verenigingen:
          - naam: stura
          - naam: politeia

      - tekst: Politieke wetenschappen
        verenigingen:
          - naam: stura
          - naam: politeia

      - tekst: Sociologie
        verenigingen:
          - naam: stura
          - naam: politeia
---
