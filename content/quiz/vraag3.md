---
vraag: Wat zijn je interesses?
type: meerkeuze
antwoorden:
  - tekst: Archeologie
    verenigingen:
      - naam: aw

  - tekst: BDSM
    verenigingen:
      - naam: kajira

  - tekst: Consulting
    verenigingen:
      - naam: oedc

  - tekst: Cultuur en creatief zijn
    vraag: Wat zijn je interesses?
    type: meerkeuze
    antwoorden:
      - tekst: Culturele voorstellingen bijwonen
        verenigingen:
          - naam: cultour
      - tekst: Dansen
        verenigingen:
          - naam: gudc
      - tekst: Dichtkunst en gesproken woord
        verenigingen:
          - naam: auwla
      - tekst: Fotografie
        verenigingen:
          - naam: schamper
      - tekst: Muziek spelen
        verenigingen:
          - naam: guho
          - naam: guso
          - naam: studentenfanfare
      - tekst: Schilderen en tekenen
        verenigingen:
          - naam: modelstudies
      - tekst: Schrijven
        verenigingen:
          - naam: schamper
          - naam: auwla
          - naam: prometheus
      - tekst: Theater
        verenigingen:
          - naam: matrak
      - tekst: Zingen
        verenigingen:
          - naam: guk

  - tekst: De pers
    vraag: Wat zijn je interesses?
    type: meerkeuze
    antwoorden:
      - tekst: Fotografie
        verenigingen:
          - naam: schamper
      - tekst: Journalistiek
        verenigingen:
          - naam: schamper
          - naam: urgent
      - tekst: Lay-out
        verenigingen:
          - naam: schamper
      - tekst: Schrijven
        verenigingen:
          - naam: schamper
          - naam: auwla
          - naam: prometheus

  - tekst: Duurzaamheid
    verenigingen:
      - naam: durgent

  - tekst: Economie
    verenigingen:
      - naam: flyse
      - naam: capitant
      - naam: re9000

  - tekst: Finance
    verenigingen:
      - naam: capitant

  - tekst: Kritisch denken en debat
    verenigingen:
      - naam: engage
      - naam: tsg

  - tekst: land- en tuinbouw
    verenigingen:
      - naam: gk
      - naam: stujardin

  - tekst: Me inzetten voor anderen
    vraag: Wat zijn je interesses?
    type: meerkeuze
    antwoorden:
      - tekst: Andere studenten ondersteunen
        verenigingen:
          - naam: flux
          - naam: cultour
          - naam: stt
      - tekst: Cultuur promoten
        verenigingen:
          - naam: cultour
      - tekst: studentenvertegenwoordiging
        verenigingen:
          - naam: gsr

  - tekst: ondernemen
    verenigingen:
      - naam: flyse

  - tekst: Politiek
    vraag: Waar ben je specifiek in geïnteresseerd?
    type: meerkeuze
    antwoorden:
      - tekst: Vlaamse en nationale politiek
        vraag: Welke overtuiging?
        type: meerkeuze
        antwoorden:
          - tekst: Vlaams-Nationalistisch
            verenigingen:
              - naam: nsv

          - tekst: Conservatief en Vlaams-Nationalistisch
            verenigingen:
              - naam: kvhv

          - tekst: Antikapitalistisch
            verenigingen:
              - naam: als

          - tekst: Christendemocratisch
            verenigingen:
              - naam: cds

          - tekst: Marxistisch
            verenigingen:
              - naam: comac

          - tekst: Ecologistisch
            verenigingen:
              - naam: groen
              - naam: durgent

          - tekst: Vlaams-republikeins
            verenigingen:
              - naam: nva

          - tekst: Socialistisch
            verenigingen:
              - naam: socialisten

          - tekst: Liberaal
            verenigingen:
              - naam: lvsv

      - tekst: Europese politiek
        verenigingen:
          - naam: minos

      - tekst: Internationale politiek, Verenigde Naties en debat
        verenigingen:
          - naam: vvny

  - tekst: STEM
    vraag: Wat zijn je interesses?
    type: meerkeuze
    antwoorden:
      - tekst: Techniek
        verenigingen:
          - naam: ieee
          - naam: best
      - tekst: Wiskunde
        verenigingen:
          - naam: prime
      - tekst: Fysica en sterrenkunde
        verenigingen:
          - naam: vvn
      - tekst: Informatica
        verenigingen:
          - naam: zeus

  - tekst: radio maken
    verenigingen:
      - naam: urgent

  - tekst: Verschillende culturen
    vraag: Wat zijn je interesses?
    type: meerkeuze
    antwoorden:
      - tekst: Armenië
        verenigingen:
          - naam: hayasa
      - tekst: Japan
        verenigingen:
          - naam: tnk
      - tekst: De Klassieke Oudheid
        verenigingen:
          - naam: kk
---
