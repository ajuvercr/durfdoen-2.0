---
vraag: Zit je op een home?
type: dropdown
antwoorden:
  - tekst: Ja
    vraag: Welke home?
    type: dropdown
    antwoorden:
      - tekst: Home Astrid
        verenigingen:
          - naam: astrid

      - tekst: Home Fabiola
        verenigingen:
          - naam: fabiola

      - tekst: Home Bertha
        verenigingen:
          - naam: bertha

      - tekst: Home Boudewijn
        verenigingen:
          - naam: boudewijn

      - tekst: Home Vermeylen
        verenigingen:
          - naam: vermeylen
          
  - tekst: Nee
---
