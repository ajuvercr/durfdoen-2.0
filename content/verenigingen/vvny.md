---
titel: Vereniging voor de Verenigde Naties Youth
id: vvny
naam: Vereniging voor de Verenigde Naties Youth
verkorte_naam: Vereniging voor de Verenigde Naties Youth
konvent: pfk
contact: unya.gent@gmail.com
website: http://www.vvn.be/vvn-youth/
social:
  - platform: facebook
    link: https://www.facebook.com/UNYAGent/
themas:
  - politiek en filosofisch
  - internationaal
---

United Nations Youth Association Gent is de jongerenafdeling van de Vereniging voor de Verenigde Naties te Gent waarbij de beginselen en principes van de VN een belangrijke leidraad zijn.
Als organisatie door en voor studenten houden we ons voornamelijk bezig met internationale thema's die op de agenda van de VN staan, organiseren we lezingen over actuele, internationale onderwerpen, ambassadebezoeken en Model United Nations 'mock' sessies.
Deze laatste is een interactieve sessie waarbij we verschillende organen van de Verenigde Naties simuleren en door "public speaking" het standpunt van een welbepaald land vertegenwoordigen.
Daarnaast gaan we met onze groep naar het buitenland om op een laagdrempelig en studentikoze manier mee te participeren aan de Model United Nations in andere landen.
Spreekt onze organisatie jou aan? Aarzel niet om ons te contacteren of spring eens binnen bij een activiteit van ons.
