---
titel: Gentse Farma Kring
id: gfk
naam: Gentse Farma Kring
verkorte_naam: Gentse Farma Kring
konvent: fk
contact: info@gentsefarmakring.be
website: http://www.gentsefarmakring.be
social: 
  - platform: facebook
    link: https://www.facebook.com/gentsefarmakring/
  - platform: instagram
    link: https://www.instagram.com/gentsefarmakring/
themas:
  -  faculteit
---

Faculteitskring voor de studenten farmaceutische wetenschappen
