---
titel: VVN
id: vvn
naam: VVN
verkorte_naam: VVN
themas:
  - wetenschap en techniek
konvent: wvk
contact: vvn@student.ugent.be
website: https://vvn.ugent.be/
social:
  - platform: facebook
    link: https://www.facebook.com/vvn.ugent
showcase:
  - photo: /assets/logos/VVNA.jpg
  - photo: /assets/logos/VVNB.jpg
  - photo: /assets/logos/VVNC.jpg
---

# Over ons
De VVN: een studentenvereniging die zich inzet om wetenschap bij het grote publiek te brengen.

Ben je het beu om doelloos door de Overpoort te lopen en alweer te eindigen met een Filippijnse en een nieuw huisdier? Zoek je wat diepgang in het goedkope studentenleven of wil je gewoon iets intellectueler overkomen bij de vrouwtjes/venten/onzijdigen? Dan heeft de Vereniging voor Natuurkunde (VVN) de ideale oplossing voor jou. De VVN organiseert namelijk door het jaar heen regelmatig populariserende lezingen in verband met fysica en sterrenkunde. Deze lezingen zijn ideaal om een bijdrage te kunnen leveren aan verschillende caféonderwerpen zoals Schrödingers kat, alternatieve tijdlijnen of klimaatopwarming. Daarnaast kunnen ze verhelderend werken bij verschillende films en series zoals Interstellar, Rick and Morty en The Big Bang Theory.

Na elke lezing is er ook een gelegenheid om het ethanolgehalte op een peil te houden dat een student waardig is. Daarbij is er ook de mogelijkheid om het nut van de lezingen te toetsen tijdens spontane oefenconversaties, die begeleid worden door fysici en/of burgelijk ingenieurs. Ook aan de enkele romantische ziel onder de studenten heeft de VVN gedacht. We organiseren sterrenkijkavonden bij maanlicht of blauw licht (afhankelijk van de speeldata van de Buffalo’s). Deze gelegenheid is ook ideaal voor spelers en mannenverslinders die `een scheun stukske vlees’ willen strikken.

We hebben onze blik echter niet alleen gericht op toekomstige relatiestatussen en succesvolle tinderdates, maar ook op het verdere voortbestaan van het studentenleven. Door het snode plan van de Chinezen om gezamenlijk de thermostaat hoger te zetten, zullen de Walen binnenkort een eigen strand ter beschikking hebben. Daarom gaat de VVN ook jaarlijks op ontdekkingsreis naar hoger gelegen locaties om te kijken of deze geschikt zijn voor verdere cognitieve ontwikkeling.

Tijdens deze reis zal de VVN verblijven op een buitenlandse locatie die onze interesse voor fysica en aanverwanten aanwakkert. Tot slot heeft de VVN ook een trend ontdekt bij verschillende studentenverenigingen waarbij onschuldige activiteiten worden georganiseerd zoals spelletjesavond, quiz, enz. om hun imago op te krikken. Wij van de VVN vonden het dan ook geen slecht idee om dat ook eens te proberen, maar dan beter. Heb je interesse in deze unieke levenservaringen, volg dan onze Facebookbladzijde of bezoek onze site: vvn.ugent.be

PS. De VVN wordt dan wel een ‘studentenvereniging’ genoemd, aan dopen en cantussen doen wij (voorlopig) niet. De VVN heeft dan ook geen leden noch lintjes, enkel een bestuur van studenten die bereid zijn zich een jaar in te zetten voor ons hoofddoel, zijnde wetenschapspopularisatie. Iedereen, student aan de UGent of niet, is welkom op onze gratis activiteiten.
