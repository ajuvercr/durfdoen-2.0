---
naam: PPSR
verkorte_naam: PPSR
titel: De Psychologische en Pedagogische StudentenRaad
id: ppsr
naam: De Psychologische en Pedagogische StudentenRaad
konvent: gsr
website: https://ppsr.ugent.be
contact: ppsr@student.ugent.be
themas:
  -  engagement
---

PPSR staat voor Psychologische en Pedagogische StudentenRaad. De benaming illustreert dat het een studentenorganisatie betreft die de stem van de studenten aan de Faculteit Pyschologie en Pedagogische Wetenschappen vertegenwoordigt. Concreet betekent dit dat de studenten van de studentenraad op verschillende vlakken actief zijn en zo bijdragen aan het beleid. Zo zetelen er onder meer studenten in de Faculteitsraad en in de verschillende commissies. Jullie studentenraad is een schakel in een goed geoliede machine die ervoor zorgt dat jullie het naar jullie zin hebben op de faculteit en op onze universiteit in het algemeen. Studenten zijn broodnodig bij het inkleuren van het beleid opdat deze instelling blijvend zou meespelen, opdat deze instelling blijvend laagdrempelig toegankelijk zou zijn voor alle gemotiveerde studenten, zowel nu als in de toekomst.
