---
titel: Geeraard
id: geeraard
naam: Geeraard
verkorte_naam: Geeraard
konvent: sk
website: https://www.degeeraard.be
social:
  - platform: instagram
  - platform: facebook
    link: https://www.facebook.com/De-Geeraard-869042639872298/
themas:
  - streek
postcodes:
  - 9500
  - 9506
---

De Geeraard is een streekclub voor de Geeraardsbergse student te Gent.
Opgericht als een kleine club in 1973, waarvan de laatste heroprichting plaatsvond in 2000, staat de Geeraard sterker dan ooit. Het prachtige rood en geel dringt alsmaar dieper binnen in het Gentse studentenleven, waar menig studentenclubs en zelfs kringen al weleens de naam ‘De Geeraard’ horen vallen heeft. Het groeiende karakter van de club is niet te wijten aan toevalligheden, maar aan het sterke samenhorigheidsgevoel dat de Geeraardsberge studenten samen sterk maakt. Elke dinsdagavond kan u ons terug vinden in onze geliefde stamcafé ’t Putje in de befaamde Overpoort, waar wij en ook onze fiere peter Bjorn u met veel enthousiasme zal ontvangen.
Wij zijn een steun en toeverlaat voor de Geeraardsbergse student, die op zoek is naar vertier buiten het intensieve studentenleven, die sociaal contact en nieuwe vriendschappen hoog in het vaandel draagt en geen schrik heeft om menig pint te drinken met iemand die hij maar half kent.
Wij voorzien ook tal van goedkope activiteiten gedurende het jaar voor en door de student. Cocktail- en jeneveravond, fifa-toernooi, vrij podium, op tijd en stond een cantus, maar ook deelname aan de sportcompetitie van het SK Ghendt (waar we tevens al enkele keren in de prijzen gevallen zijn)
