---
titel: LVSV
id: lvsv
naam: LVSV
verkorte_naam: LVSV
konvent: pfk
contact: voorzitter@lvsvgent.be
website: https://www.lvsvgent.be/
social:
  - platform: facebook
    link: https://www.facebook.com/LVSVGent/
  - platform: twitter
    link: https://twitter.com/ArcusCoTangens
themas:
  -  politiek en filosofisch
---

Het Liberaal Vlaams Studentenverbond (LVSV) Gent is een politiek-filosofische studentenvereniging, reeds actief sinds 1930.
Pluralisme, openheid en partijonafhankelijkheid zijn onze voornaamste kenmerken. Via debatten, discussieavonden en lezingen trachten wij de Gentse student warm te maken voor een filosofisch onderbouwd liberalisme.
Het studentikoze krijgt echter ook onze aandacht, aan de hand van cantussen, bierbowlings en uitstapjes verkennen we ook de minder formele gebieden van het Gentse studentenleven.
