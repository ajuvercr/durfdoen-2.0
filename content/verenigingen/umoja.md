---
titel: Umoja Gent
id: umoja
naam: Umoja
verkorte_naam: Umoja
konvent: ik
contact: umojagent@gmail.com
social:
  - platform: facebook
    link: https://www.facebook.com/UmojaGent9200/
themas:
  -  internationaal
showcase:
  - photo: /assets/logos/umojaA.jpeg
  - photo: /assets/logos/umojaB.jpeg
  - photo: /assets/logos/umojaC.jpeg 
---

Umoja Gent is een Afrikaanse studentenvereniging die de wil heeft om de Afrikaanse diversiteit in Gent zowel te vertegenwoordigen als uit te dragen. Umoja is afkomstig uit het Swahili en betekent letterlijk “eenheid”. Om eenheid te creëren onder alle studenten is het van belang dat iedere student zich kan identificeren met een studentenvereniging. Wij zijn dus de stem van een belangrijke, maar ondervertegenwoordigde groep van studenten.

Umoja wil hier graag een bijdrage aan leveren door een platform te bieden om de verscheidenheid aan Afrikaanse culturen te promoten. Daarnaast zullen actuele maatschappelijke thema’s zoals sociale ongelijkheid, dekolonisatie en emancipatie ook niet onbesproken blijven. Het is de bedoeling om de maatschappelijke realiteit waarmee studenten met een Afrikaanse migratieachtergrond  geconfronteerd worden, bespreekbaar te maken in het studentenleven. Eveneens streven wij naar interculturele samenhorigheid onder alle Gentse studenten.

Umoja creëert een inclusieve safe space voor elke student die daar nood aan heeft!
