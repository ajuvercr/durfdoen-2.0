---
titel: Auw La
id: auwla
naam: Auw La
verkorte_naam: Auw La
konvent: kultk
contact: auwlagent@gmail.com
social:
  - platform: facebook
    link: https://www.facebook.com/AuwLaGent/
themas:
  -  cultuur
showcase:
  - photo: /assets/logos/AuwLaA.jpg
  - photo: /assets/logos/AuwLaB.jpg
  - photo: /assets/logos/AuwLaC.jpg
---

Auw La is sinds 2016 lid van het Kultureel Konvent. Auw La biedt beginnende poëten, slammers, schrijvers, rappers en woordfanaten een platform om hun passie te delen en hun teksten te performen. Auw La is voor iedereen die iets te zeggen heeft. Gooi je woorden op de planken van de aula. En als die stap nog te groot is, nestel je dan mee met ons op het kot van de maand en geniet van poëzie en straffe taal.
