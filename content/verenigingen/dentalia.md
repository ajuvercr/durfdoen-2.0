---
titel: Dentalia
id: dentalia
naam: Dentalia
verkorte_naam: Dentalia
konvent: fk
contact: dentaliaghent@gmail.com
website: https://www.dentalia.be
social:
  - platform: facebook
    link: https://www.facebook.com/Dentalia/
  - platform: instagram
    link: https://www.instagram.com/dentalia_gent/
themas:
  -  faculteit
showcase:
  - photo: /assets/logos/DentaliaA.jpg
  - photo: /assets/logos/DentaliaB.jpg
  - photo: /assets/logos/DentaliaC.jpg
---

Dentalia is dé studentenvereniging bij uitstek voor alle tandartsen in spé. Door de gezellige grootte van onze studierichting, bestaat een hechte band tussen alle leden; iedereen kent iedereen! Over de jaren heen smeed je hechte vriendschappen die levenslang meegaan. Tenslotte worden we ook elkaars collega’s.
Waar de UGent stopt, gaat Dentalia verder. Dit door het organiseren van tal van feest- en sportactiviteiten, waaronder het jaarlijkse Galabal, een kaas- & wijnavond, de legendarische sneukeltochten, een tweejaarlijkse sportdag met onze Leuvense collega's en uiteraard de onvergetelijke cantussen. Ook met bevriende kringen slaan we de handen in elkaar: Doctors VS Dentists, 12-urenloop, zwemmarathon, Halloweenparty met VTK, dagdisco met de UZ-kringen,… Wij zorgen voor het nodige vertier en afleiding om de zorgen van het dagelijks leven in de P8 te vergeten. Je bent méér dan welkom op een activiteit om van ons charmant studentleven te proeven. 

