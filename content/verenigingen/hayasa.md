---
titel: Hayasa
id: hayasa
naam: Hayasa
verkorte_naam: Hayasa
contact: hayasa@student.ugent.be
website: http://www.hayasa.be/
social: 
  - platform: facebook
    link: https://www.facebook.com/HayasaBelgium/
  - platform: instagram
    link: https://www.instagram.com/hayasa.be/
themas:
  -  diversiteit
  -  internationaal
konvent: ik
---

Hayasa is an Armenian student organisation called to unite Armenian students of Belgium with the goal of preserving and promoting Armenian historical and cultural values. We seek to learn more about our culture but also to bring it to others.
Furthermore, hayasa encourages Armenians to learn the Armenian language and history and strengthen the ties with our motherland. Besides that we also cooperate with similar bodies in Armenia and Diaspora.
