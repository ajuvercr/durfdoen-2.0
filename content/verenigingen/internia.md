---
titel: Internia
id: internia
naam: Internia
verkorte_naam: Internia
konvent: sk
website: https://internia.be/
social:
  - platform: facebook
    link: https://www.facebook.com/Internia-339215779503916/
themas:
  - streek
postcodes:
  - 8520
  - 8530
  - 8700
  - 8710
  - 8720
  - 8740
  - 8750
  - 8755
  - 8760
  - 8770
  - 8780
  - 8810
---

Internia staat voor internaat in actie. Onze kleuren zijn Groen-Zwart. Wij zijn opgericht in 1991, maar dit is zeker niet het einde van het verhaal. Zoals de naam doet vermoedden waren wij oorspronkelijk een internaat club. Bij het verdwijnen van het internaat besloten enkele jonge enthousiaste leden dat dit zeker niet het einde van de club mocht zijn. In 2000 werd Internia onder leiding van Benny Demonie, omgevormd tot een West-Vlaamse club. Na vele jaren groeien en een traditie van mensen uit alle uithoeken van West-Vlaanderen samen te brengen, zaten wij nog niet in onze finale vorm. Gezien wij niet de enige West-Vlaamse club waren in Gent moesten er namelijk afspraken gemaakt worden om regio's te verdelen. Ook bij deze drastische verandering voor Internia hielden wij het hoofd koel. De regio Tielt en zeer veel omstreken (Tielt, Pittem, Kuurne, Harelbeke, Wielsbeke, Oostrozebeke, Dentergem, Wingene, Ruiselede, Meulebeke, Lichtervelde en Ingelmunster) werden onder onze zachte en bekwame maar vooral studentikoze vleugels toevertrouwd. Wij zijn vanaf dag 1 tot na blijven groeien en zijn nu een club van ongeveer 50 actieve leden. Met elke maandag een clubavond in ons stam café "Salto" en daarnaast nog 1 andere activiteit per week weten we iedereen zeker aan zij trekken te laten komen.
