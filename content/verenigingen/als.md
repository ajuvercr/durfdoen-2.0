---
titel: Actief Linkse Studenten
id: als
naam: Actief Linkse Studenten
verkorte_naam: Actief Linkse Studenten
konvent: pfk
themas:
  -  politiek en filosofisch
website: https://actieflinks.be/
contact: als@socialisme.be
social:
- platform: facebook
  link: https://www.facebook.com/Actieflinks/
---

De Actief Linkse Studenten (ALS) is een antikapitalistische jongerenorganisatie die strijd voert tegen seksisme, extreemrechts, klimaatverandering en het onderliggende kapitalistische systeem dat in steeds diepere crisis verzeild.
Doorheen het jaar organiseren we tal van activiteiten. We gaan naar of organiseren zelf acties en betogingen waar we onze ideeën en alternatieven aanbrengen.
We voeren campagnes rond verschillende thema's en organiseren politieke meetings om te discussiëren over de actuele problemen en hoe er strijd tegen te organiseren, maar ook historische meetings over strijd bewegingen in het verleden.
Als activistische studentenvereniging proberen we dan ook niet enkel te discussiëren over maatschappijverandering maar zetten we de strijd ook in de praktijk om.
