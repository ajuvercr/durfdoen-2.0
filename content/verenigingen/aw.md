---
titel: Archeologische Werkgroep
id: aw
naam: Archeologische Werkgroep
verkorte_naam: Archeologische Werkgroep
konvent: wvk
contact: aw.ugent@gmail.com
website: http://aw-ugent.be/
social:
  - platform: facebook
themas:
  - cultuur
  - wetenschap en techniek
---

De Archeologische Werkgroep is een vereniging die zich vooral richt op de studenten archeologie. Voor hen proberen we allerlei activiteiten te organiseren, of toegankelijker te maken, die aansluitend en verruimend zijn voor onze studies! Deze activiteiten houden onder andere in dat we musea, archeologische sites en andere historische plaatsen bezoeken tijdens onze leuke uitstapjes. Ook door belangrijke lezingen en contactdagen toegankelijker te maken helpen we studenten om op de hoogte te blijven van de laatste onderzoeken binnen het vakgebied.
Naast de serieuzere activiteiten blijven we natuurlijk ook een vereniging voor studenten, die wel eens nood hebben aan ontspanning! Daarom organiseren we zelf dan ook allerlei andere leuke en ontspannende activiteiten zoals de “archeologische salons”, Zomerse uitstapjes en trektochten, try-outs in schermen en andere sporten, de Gentse cafés gaan bezoeken,…
Naast onze activiteiten is de Archeologische Werkgroep ook trotse uitgever van ‘het Sleufje’, hét archeologisch studentenblad bij uitstek. Daarin komen jullie de laatste nieuwtjes van de opleiding te weten, kunnen onze (al dan niet) zelfgeschreven super interessante en leuke artikels lezen, kan je je in de les bezighouden met de puzzels, raadseltjes en quizzen op te lossen, of kun je iets terugvinden over de laatste avonturen van de AW! Jullie krijgen al deze lectuur dan ook nog eens gratis, enkele keren per academiejaar!
Om dit alles in goede banen te leiden probeert de AW zoveel mogelijk samen te komen om te vergaderen. Op deze vergaderingen is iedereen steeds welkom. Met een lekker drankje en hapje, bij het gezellige haardvuur, worden dan de relevante en minder relevante zaken van onze vereniging en alles die zich in de wereld en daarbuiten afspeelt besproken.
