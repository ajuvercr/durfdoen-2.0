---
titel: Klassieke Kring
id: kk
naam: Klassieke Kring
verkorte_naam: Klassieke Kring
konvent: wvk
website: http://www.klassiekekring.be/
social:
  - platform: facebook
    link: https://www.facebook.com/klassiekekring/
themas:
  -  cultuur
showcase:
  - photo: /assets/logos/KlassiekeKringA.jpg
  - photo: /assets/logos/KlassiekeKringB.jpg
  - photo: /assets/logos/KlassiekeKringC.jpg
---

De Klassieke Kring, kortweg KK, is een studentenvereniging, gericht op de Taal- en Letterkundestudenten Twee Talen die Latijn en/of Grieks als opleiding aan de Universiteit Gent volgen of gevolgd hebben.

De KK is een pluralistische studentenvereniging, die boven alle politiek staat, doch de belangen behartigt van de klassieke talen in het algemeen, en die van alle studenten Latijn en/of Grieks aan de Universiteit Gent in het bijzonder. Ook niet-studenten Grieks en/of Latijn zijn welkom. We verwelkomen iedereen met een hart voor de klassieke talen.

De KK verplicht zich ertoe de studenten te verenigen in vriendschap en verdraagzaamheid, en heeft aandacht voor de verschillende aspecten van het studentenleven. De kring stelt zich tot doel de samenwerking tussen het academisch, het administratief, het technisch personeel en de studenten Latijn en/of Grieks te bevorderen. Dit gebeurt buiten het lesverband, meer bepaald via het organiseren van feestelijkheden, cultuuruitstappen, sportactiviteiten en historisch-maatschappelijke voordrachten. De KK organiseert in samenwerking met de universiteit tevens activiteiten van algemeen nut, zoals de infodag voor toekomstige studenten en de introductiedag aan het begin van het academiejaar. De KK staat waar mogelijk de studenten bij.
