---
titel: FLUX
id: flux
naam: FLUX
verkorte_naam: FLUX
konvent: wvk
contact: flux@student.ugent.be
website: http://student.ugent.be/flux/
themas:
  -  lifestyle
---

Flux is een non-profit studentenvereniging verbonden aan de Universiteit Gent die sinds 2003 actief is in Gent en zich richt tot alle studenten. Ons doel is ook de studenten te ondersteunen en te begeleiden tijdens hun studies. We richten ons ook naar de studenten met een migratie achtergrond om hun studie-loopbaan te versoepelen. Naast begeleiding, hulp en andere projecten, organiseren we ook evenementen om ons te amuseren.

We kijken er naar uit om kennis te maken met je. Hopelijk tot binnenkort op één van onze activiteiten!
