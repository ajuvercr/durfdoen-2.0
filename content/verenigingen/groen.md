---
naam: Jong Groen StuGent
verkorte_naam: Jong Groen StuGent
titel: JGroen StuGent
id: groen
naam: JGroen StuGent
konvent: pfk
contact: stugent@jonggroen.be
website: https://www.jonggroen.be/stugent
social:
  - platform: facebook
    link: https://www.facebook.com/Jong-Groen-StuGent-794541110577767/
themas:
  -  politiek en filosofisch
---

Jong Groen StuGent is een alternatieve vereniging die zich op studenten met een inzet voor een duurzame en sociaal rechtvaardige maatschappij. We maken via debatten en lezingen ruimte voor een kritische benadering van culturele en wetenschappelijke vragen.
Hierbij zetten we graag onze deuren open voor studenten van verschillende (politieke) strekkingen, maatschappelijke achtergronden en overtuigingen.
Verder blijft het bij ons niet alleen bij een babbel maar nemen we ook deel aan concrete protesten, opruimacties en andere activiteiten.
Ben je student en wil je mee je mening laten horen, je handen uit de mouwen steken of interessante lezingen bijwonen? Contacteer ons dan gerust via onze FB-pagina of spreek ons aan na een activiteit.Bouw mee aan de toekomst, want ze is van ons!
