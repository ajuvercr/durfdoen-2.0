---
titel: Vlaamse Psychologische en Pedagogische Kring
id: vppk
naam: Vlaamse Psychologische en Pedagogische Kring
verkorte_naam: Vlaamse Psychologische en Pedagogische Kring
konvent: fk
website: https://www.vppk.be
contact: praeses@vppk.be
themas:
  -  faculteit
---
Als VPPK – studentenvereniging van de Faculteit Psychologie en Pedagogische Wetenschappen (UGent) – engageren wij ons om een waardevolle bijdrage te leveren aan het studentenleven. Zo voorzien wij jaarlijks duizenden studenten van hun boeken en cursussen, organiseren wij talloze soorten activiteiten en zorgen wij ervoor dat studenten over heel Gent kunnen genieten van allerlei voordelen.
