---
titel: Hermes
id: hermes
naam: Hermes
verkorte_naam: Hermes
konvent: fk
website: https://www.hermesgent.be
social: 
  - platform: linkedin
    link: https://www.linkedin.com/company/hermesgentvzw/
  - platform: instagram
    link: https://www.instagram.com/hermesgent/
  - platform: facebook
    link: https://www.facebook.com/Hermesgent/
themas:
  -  faculteit
---

Studeer je Industriële Wetenschappen, dan heb je ons zeker al zien passeren. We voorzien jullie cursussen, zorgen voor nuttige infoavonden en een groot job- en stage-event.
Natuurlijk gaat studeren niet enkel om serieus zijn, dus organiseren wij ook tal van randactiviteiten voor gedoopte en ongedoopte leden. Dit gaat van sporten zoals voetbal, kajakken en VR-gamen tot cultuur activiteiten zoals optredens, kroegentochten en natuurlijk gezellige clubavonden.
Hoewel we een kring zijn die vasthangt aan een opleiding, zijn er enorm veel leden die eigenlijk niets met Industriële Wetenschappen te maken hebben. Farmacie, Geneeskunde, handelswetenschappen en Bio-wetenschappen zijn allemaal aanwezig in ons ledenbestand. Wees dus zeker niet bang om eens goeiendag te komen zeggen!
