---
titel: Vlaamse Technische Kring
id: vtk
naam: Vlaamse Technische Kring
verkorte_naam: Vlaamse Technische Kring
konvent: fk
website: https://vtk.ugent.be
contact: vtk@vtk.ugent.be
social:
  - platform: facebook
  - platform: instagram
    link: https://www.instagram.com/vtkgent/
  - platform: linkedin
    link: https://www.linkedin.com/company/vtk-gent-vzw/
  - platform: youtube
    link: https://www.youtube.com/channel/UCkVWU6oSMq0IZWHbOa7WrVw
themas:
  -  faculteit
showcase:
  - photo: /assets/logos/VTKA.jpg
  - photo: /assets/logos/VTKB.jpg
  - photo: /assets/logos/VTKC.jpg
---
De Vlaamse Technische Kring is de overkoepelende kring voor alle studenten burgerlijk ingenieur en burgerlijk ingenieur-architect aan de universiteit van Gent. Jaarlijks worden er tal van evenementen georganiseerd. Deze kunnen zowel cultureel, feestelijk, sportief alsook educatief en job-gerelateerd zijn.

Zo is de Vlaamse Technische Kring zeker interessant voor mensen die veel belang hechten aan interessante jobmogelijkheden! Deze ambitieuze vereniging organiseert verschillende grootse activiteiten, zoals bijvoorbeeld de VTK Jobfair. Dit is de ultieme plaats om de eerste belangrijke contacten te leggen met een hele reeks bedrijven en waar je misschien je toekomstige job zal vinden.

Daarbovenop blinkt VTK ook uit met hun eigen café in het hartje van overpoort, Delta. Hier worden er niet alleen talrijke feestjes georganiseerd maar ook gezellige winetastings in de lounge. Tot slot verzorgt VTK ook de boekenverkoop. Meer info? Surf naar www.vtk.ugent.be.'
