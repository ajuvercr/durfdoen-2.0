---
naam: FRiS
verkorte_naam: FRiS
titel: Faculteitsraad Ingenieurswetenschappen en Architectuur
id: fris
naam: Faculteitsraad Ingenieurswetenschappen en Architectuur
konvent: gsr
contact: fris@ugent.be
website: https://fris.ugent.be
themas:
  -  engagement
---
FRiS staat voor Facultaire Raad van Ingenieursstudenten, ofwel de overkoepelende raad van alle studentenvertegenwoordigers van de Faculteit Ingenieurswetenschappen en Architectuur. Onze studentenvertegenwoordigers komen van drie campussen, namelijk Kortrijk, Plateau en Schoonmeersen. FRiS verdedigt dus de belangen van zowel de burgerlijk ingenieurs, de burgerlijk ingenieur-architecten als de industrieel ingenieurs!
Wat houdt dat dan in, ‘de belangen van de studenten verdedigen’? Dit kan gaan van het zorgen voor een gebruiksvriendelijker facultair rekentoestel, het aanpassen van de 7/20 regel of het verlagen van het aantal NPGE’s tot zelfs het schrijven van standpunten over zaken als het bekendmaken van de punten. Kortom, als studentenvertegenwoordiger kan je mee de toekomst van de faculteit bepalen!
