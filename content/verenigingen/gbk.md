---
titel: Gentse Biologische Kring
id: gbk
naam: Gentse Biologische Kring
verkorte_naam: Gentse Biologische Kring
konvent: fk
contact: praesidium@biologie-gent.be
themas:
  -  faculteit
---

De Gentse Biologische Kring is een kleine maar hechte vriendengroep rond de richting Biologie, die ervoor zorgt dat de studenten zich daar zo welkom mogelijk voelen.

Hun proefbuis-jeneverfuif is een graag geproefd concept waar elk jaar meer en meer smaak naar is. Als je je op maandagavond verveelt, spring dan eens binnen in the Porter House! Ze hebben daar namelijk hun wekelijkse clubavonden.
