---
titel: Moeder Oilsjterse
id: oilsjterse
naam: Moeder Oilsjterse
verkorte_naam: Moeder Oilsjterse
konvent: sk
website: http://www.moederoilsjterse.be/
themas:
  - streek
postcodes:
  - 9300
  - 9308
  - 9310
  - 9320
  - 9340
  - 9420
  - 9450
  - 9451
---

Moeder Oilsjterse probeert de mannelijke Aalsterse studenten te Gent te verenigen en om onder hen hechte vriendschapsbanden te smeden of deze te onderhouden. Dankzij haar Aalsterse regionaal karakter kan Moeder Oilsjterse ertoe bijdragen dat afgestudeerd zijn niet betekent dat men zijn clubvrienden niet terugziet.
