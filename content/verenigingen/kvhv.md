---
titel: KVHV
id: kvhv
naam: KVHV
verkorte_naam: KVHV
konvent: pfk
website: https://gent.kvhv.org/
social:
  - platform: twitter
    link: https://twitter.com/kvhvgent
  - platform: facebook
    link: https://www.facebook.com/pages/KVHV-Gent/155474026518
themas:
  -  politiek en filosofisch
---

Het Katholiek Vlaams Hoogstudentenverbond Gent is een kleurendragende academische gemeenschap die zich inhoudelijk plaatst rond nationaal-conservatieve principes. Wij willen menselijk kapitaal verenigen in een avant-gardistische, elitevormende levensgemeenschap die de soevereiniteit en de emancipatie van onze leden en van onze volksgemeenschap nastreeft.
Verder zijn we een Vlaamsgezinde vereniging die zich inzet voor de Vlaamse ontvoogdingsstrijd en het behoud van onze cultuur, waarden, normen en tradities. We willen onze leden politiek en intellectueel klaarstomen zodat ze met burgerzin in de samenleving staan. Door het organiseren van lezingen, het schrijven van opiniestukken en deelname in het maatschappelijk debat met als doel de publieke opinie te beïnvloeden. Het stichtingsjaar van onze vereniging is 1887, wat ons één van de oudste nog actieve studentenverenigingen in Vlaanderen maakt. Het Verbond heeft niet alleen een politieke maar ook een studentikoze pijler.
Als uitgever van de Vlaamse studentencodex spelen wij een voortrekkersrol in het Vlaams studentenleven. Wij zijn dan ook gekend voor een uitgebreide liederenkennis, internationale contacten, prachtige cantussen, traditionele kleurendracht en stijlvolle studentikoziteit
