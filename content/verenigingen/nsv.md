---
titel: NSV! Gent
id: nsv
naam: NSV! Gent
verkorte_naam: NSV! Gent
konvent: pfk
contact: nsv.gent@gmail.com
social:
  - platform: facebook
    link: https://www.facebook.com/NationalistischeStudentenverenigingGent/
themas:
  -  politiek en filosofisch
---

De Nationalistische Studentenvereniging ontstond in de jaren '70. Zoals de naam doet vermoeden is onze missie om het nationalisme op een principiële en radicale manier uit te dragen. Als NSV! kiezen we resoluut voor de Vlaamse onafhankelijkheid. Elk volk heeft immers het recht om haar eigen lot te bepalen. Elk volk heeft het recht op een eigen natie. Ook het onze.
Het nationalisme is echter meer dan enkel separatisme. Als nationalisten willen we onze eigenheid behouden. We zijn geen grijze kosmopolieten die er overal ter wereld hetzelfde uit zien - en we zijn evenmin individualisten die enkel losse verbanden met hun medemens aangaan. Voor ons is het individu steeds geworteld in verschillende gemeenschappen: het gezin, het volk, de cultuur.
Om onze leden te vormen tot idealistische, maar ook kritische voordenkers van deze ideologische optiek organiseren we tal van debatten, lezingen en discussieavonden.
Verder verdedigen we onze waarden en idealen door middel van ludieke of serieuze acties en manifestaties.
Daarnaast is de NSV! ook een studentenvereniging in de meest zuivere betekenis van het woord. Cantussen, zangavonden, gemoedelijk keuvelen op café met een pint in de hand, het hoort er allemaal bij! Voor ons betekent studentikoziteit in de eerste plaats kameraadschap. Waar we echter niet aan meedoen is het vernederen van schachten, smerige dopen of schabouwelijke braspartijen. De NSV! draagt zijn studentikoze karakter stijlvol uit
