---
titel: PRIME
id: prime
naam: PRIME
verkorte_naam: PRIME
konvent: wvk
website: http://prime.ugent.be/
contact: bestuur@prime.ugent.be
social: 
  - platform: facebook
    link: https://www.facebook.com/groups/51746335405/
themas:
  - wetenschap en techniek
---

PRIME is een vereniging aan de UGent die zich richt tot studenten die zich in wiskunde interesseren. PRIME organiseert een gevarieerd gamma aan recreatieve wiskundige activiteiten zonder te werken met leden - en dat bijna altijd gratis.
