---
titel: PKarus
id: pkarus
naam: PKarus
verkorte_naam: PKarus
konvent: wvk
contact: hello@pkarus.be
website: https://www.pkarus.be/
social:
  - platform: facebook
    link: https://www.facebook.com/PKarus.Gent/
  - platform: linkedin
    link: https://www.linkedin.com/in/pkarus/
  - platform: instagram
    link: https://www.instagram.com/pkarusgent/
themas:
  - wetenschap en techniek
showcase:
  - photo: /assets/logos/PkarusA.jpg
  - photo: /assets/logos/PkarusB.jpg
  - photo: /assets/logos/PkarusC.jpg
---

PKarus is een vereniging door en voor studenten burgerlijk ingenieur werktuigkundig-elektrotechniek aan de UGent.
Het doel van de vereniging is om de jonge ingenieurs, naast de brede opleiding aan de universiteit, ‘net iets meer’ mee te geven. De hoofdgedachte achter de oprichting van PKarus ligt in de omzetting van theorie in praktijk.
