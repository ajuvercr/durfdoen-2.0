---
naam: StuW
verkorte_naam: StuW
titel: Studentenraad Faculteit Wetenschappen
id: stuw
naam: Studentenraad Faculteit Wetenschappen
konvent: gsr
website: https://stuw.ugent.be/
contact: stuw@ugent.be
social:
  - platform: facebook
    link: https://facebook.com/stuwugent
themas:
  -  engagement
---

StuW is de Studentenraad van de Faculteit Wetenschappen. Wij vertegenwoordingen studenten op zowel facultair als universitair niveau. Dit laatste gebeurt via de Gentse Studentenraad (GSR). Op facultair niveau proberen wij studenten te verenigingen die zetelen in een van de vele commissies en raden die de universiteit en faculteit rijk is. Enkele voorbeelden hiervan zij de faculteitsraad, de bibliotheekcommissies en de vele opleidings commissies.
