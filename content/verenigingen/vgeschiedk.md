---
titel: Vlaamse Geschiedkundige Kring
id: vgeschiedk
naam: Vlaamse Geschiedkundige Kring
verkorte_naam: Vlaamse Geschiedkundige Kring
konvent: fk
website: https://www.vgkgent.com/
contact: vgkgent@gmail.com
themas:
  -  faculteit
---
De Vlaamse Geschiedkundige Kring is de vereniging voor alle studenten geschiedenis aan de Universiteit Gent. Haar thuishaven bevindt zich in de Blandijn, één van de centrale punten van de faculteit Letteren en Wijsbegeerte. De VGK is de ideale organisatie om alle verschillende aspecten van het studentenleven te ontdekken. Zo organiseert ze elk jaar tal van cultuuractiviteiten en neemt ze elk jaar deel aan de Nacht van de Geschiedenis van het Davidsfonds. Ze voorziet ook de vele studentencursussen, die al menig student een zorgeloze augustus bezorgden. Ook op sportief en feestelijke vlak staat ze er elk jaar opnieuw. Zo organiseert ze dit jaar opnieuw een openingsfuif in de Vooruit. Als kers op de taart biedt ze jaarlijks aan haar leden twee culturele reizen en een skireis aan.
