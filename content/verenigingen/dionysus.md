---
titel: Dionysus
id: dionysus
naam: Dionysus
verkorte_naam: Dionysus
konvent: sk
website: http://dionysus.skghendt.be/
contact: studentenclub.dionysus@gmail.com
social:
  - platform: facebook
    link: https://www.facebook.com/HSCDionysus/
  - platform: instagram
    link: https://www.instagram.com/studentenclub.dionysus/
themas:
  - streek
postcodes:
  - 8400
  - 8420
  - 8421
  - 8430
  - 8431
  - 8432
  - 8433
  - 8434
  - 8450
  - 8460
  - 8470
---

HSC Dionysus is een vereniging voor studenten uit Oostende en omstreken in Gent. Wij zijn een toffe bende die tracht mensen dichter bij elkaar te brengen. Naast de typische studentikoze activiteiten zetten we ook steeds ons beste beentje voor bij sport- en cultuurevenementen. Zo behaalden we afgelopen academiejaar een tweede plaats in de sportcompetitie van SK Ghendt. Een vaste waarde binnen de club is de vijfjaarlijkse ‘vatrolling’ ter ere van de oprichting van Hsc Dionysus. Tijdens deze lustrumactiviteit rollen wij dus te voet een vat van Oostende naar Gent. De aankomst in de Overpoort is elke editie een groot feest in gezelschap van andere SK0-clubs.
