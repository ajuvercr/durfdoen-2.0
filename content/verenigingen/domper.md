---
titel: Moeder Domper
id: domper
naam: Moeder Domper
verkorte_naam: Moeder Domper
konvent: sk
social:
  - platform: facebook
    link: https://www.facebook.com/MoederDomper
themas:
  - streek
postcodes:
  - 9400
  - 9401
  - 9402
  - 9403
  - 9404
  - 9406
  - 9470
---

De studentenvereniging die Gentse studenten verenigt die afkomstig zijn uit de wijde omgeving van Ninove.
