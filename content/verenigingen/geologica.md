---
titel: Geologica
id: geologica
naam: Geologica
verkorte_naam: Geologica
konvent: fk
contact: geologica.gent@gmail.com
social: 
  - platform: facebook
  - platform: instagram
    link: https://www.instagram.com/geologica.ugent/
website: http://www.fkgent.be/geologica/
themas:
  -  faculteit
---

Geologica is dé faculteitskring voor studenten Geologie aan de Universiteit Gent en sympathisanten. Geologica heeft vijftien enthousiaste praesidiumleden die ervoor zorgen dat de Gentse geologen naast studeren ook nog kunnen genieten van wat sociaal contact, “memorabele” feestjes, sport en cultuur! Geologica is gesticht in september 1968 wat betekent dat we enkele jaren geleden onze 50ste verjaardag vierden. Maar het is onze missie om dit jaar even memorabel te maken. Tussen de lessen door vind je ons vaak in het geokot, ons studentenlokaal in S8 dat we delen met de ‘geliefde’ geografen en landmeters. Verder hebben we ook nauw contact met de andere wetenschapskringen waarmee we doorheen het jaar enkele activiteiten delen.
