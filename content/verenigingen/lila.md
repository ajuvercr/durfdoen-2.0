---
naam: LILA
verkorte_naam: Landbouwindustrieën en landbouw
titel: Levenslange Inzet voor Leute en Ambiance
id: lila
naam: Levenslange Inzet voor Leute en Ambiance
konvent: fk
contact: praesidiumlila@gmail.com
website: https://studentenclublila.be/
themas:
  -  faculteit
---

De LILA is de kring die verbonden is met de studierichting Biowetenschappen. Lila staat voor Landbouwindustrieën en Landbouw. Gedurende je studiejaren zorgen wij voor inspanning en ontspanning. Wekelijks zorgen wij voor toffe activiteiten die je niet mag missen. Alsook zorgen wij voor cursussen en voorbeeldexamens, zodat je een optimale slaagkans hebt.
