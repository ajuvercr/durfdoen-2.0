---
naam: StuReC
verkorte_naam: StuReC
website: https://sturec.ugent.be
contact: sturec@ugent.be
social:
  - platform: facebook
    link: https://www.facebook.com/sturecfaculteitrechtencriminologie/
titel: Facultaire studentenraad rechtsgeleerdheid
id: fre
naam: Facultaire studentenraad rechtsgeleerdheid
konvent: gsr
logo: /assets/sfeerfotos/logosturec.png
themas:
  - engagement
---
StuReC is de Facultaire Studentenraad voor Rechten en Crimiologie. StuReC werkt samen met de facultaire studentenvertegenwoordigers en staat in contact met de faculteitskringen (VRG en Lombrosiana). Op basis standpunten en nota’s, ingegeven door de vragen en noden van de studenten, tracht StuReC problemen aan te kaarten of zaken te faciliteren. Deze standpunten en nota’s worden meegegeven aan de facultaire studentenvertegenwoordigers die in de bevoegde organen deze zaken aankaarten en hiervoor een oplossing proberen te voorzien.
