---
titel: Vlaamse Levenstechnische Kring
id: vlk
naam: Vlaamse Levenstechnische Kring
verkorte_naam: Vlaamse Levenstechnische Kring
konvent: fk
website: https://www.boerekot.be
contact: info@boerekot.be
social:
  - platform: facebook
    link: https://www.facebook.com/boerekot/
  - platform: instagram
    link: https://www.instagram.com/vlkgent/
themas:
  -  faculteit
showcase:
  - photo: /assets/logos/VLKA.jpg
  - photo: /assets/logos/VLKB.jpg
  - photo: /assets/logos/VLKC.jpg
---
De VLK is de vereniging voor studenten Bio-ingenieurswetenschappen. Ze hebben heuse streekbierenavonden, geweldige (paviljoen)fuiven, en zijn serieus in hun deelname aan de Bloedserieusweek en de 12urenloop! Qua cultuur loopt de VLK ook over: met de danslessen, bierbrouwsessies, de kerstmarkt en vele andere activiteiten hebben ze voor elk wat wils. Daarbij regelen ze de cursusverkoop voor alle studenten aan de campus Coupure, en organiseren ze heel wat evenementen om masterstudenten voor te bereiden op hun werkleven. CV-trainingen, lezingen, bedrijfspresentaties, het grote Bioscience Engineering Career Event en vele anderen waarborgen de toekomst van elke bio-ingenieur.
