---
titel: Wase Club
id: wase
naam: Wase Club
verkorte_naam: Wase Club
konvent: sk
website: http://www.waseclub.be/
social:
  - platform: facebook
    link: https://www.facebook.com/waseclub/
themas:
  - streek
postcodes:
  - 2070
  - 9100
  - 9111
  - 9112
  - 9120
  - 9140
  - 9150
  - 9160
  - 9170
  - 9180
  - 9190
  - 9250
---

Als student (in Gent) is het belangrijk om de banden met je geboortestreek, het Waasland, te behouden. De Wase Club is daarom de ideale studentenclub om dit te doen. Wij zorgen ervoor dat je nieuwe mensen leert kennen of oude bekenden tegen het lijf loopt en de studentenstad Gent op een studentikoze manier wordt verkend.
De Wase club is tevens de oudste club van Gent met een enorme geschiedenis die meer dan 90 jaar in de tijd teruggaat. Hierdoor heeft de Wase Club een aantal prachtige tradities opgebouwd en een resem aan leuke activiteiten. Als club organiseren we deze activiteiten zowel in Gent als in het Waasland. Hierbij krijg je de kans om de leden en oud-leden beter te leren en je ongelooflijk te amuseren in een broederlijke sfeer. Elke week is er een clubavond waarop gezellig iets kan worden gedronken. Deze gaan elke dinsdag door in het Delirium Tremens café in de Overpoortstraat om 21. Geïnteresseerde lieden kunnen op deze avond altijd een kijkje komen nemen en ze zullen warm worden onthaald door onze leden.
