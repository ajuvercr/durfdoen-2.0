---
titel: IEEE SB
id: ieee
naam: IEEE SB
verkorte_naam: IEEE SB
konvent: wvk
website: http://www.student.ugent.be/ieee/
social: 
  - platform: facebook
    link: https://www.facebook.com/ieeesbgent
themas:
  -  wetenschap en techniek
---

IEEE Student Branch Gent is de lokale afdeling van IEEE aan de UGent. We brengen hoofdzakelijk ingenieursstudenten elektrotechniek, computerwetenschappen en werktuigkunde samen, maar andere studenten met een interesse voor techniek zijn natuurlijk ook altijd welkom! We bieden een gevarieerd aanbod: van een technisch-wetenschappelijke evenementen (elektrische kart, lezingen,…) tot meer cultureel-sociale activiteiten (poolen, iemand?). Al onze evenementen geven een geweldige gelegenheid om studenten uit de andere disciplines te leren kennen!
