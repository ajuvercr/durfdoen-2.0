---
titel: Rethinking Economics
id: re9000
naam: Rethinking Economics Ghent
verkorte_naam: RE9000
konvent: wvk
contact: re9000@student.ugent.be
website: https://www.rethinkingeconomics.gent
social:
  - platform: faceboook
    link: https://www.facebook.com/RethinkingEconomicsGhent/  
themas:
  - economie en development
showcase:
  - photo: /assets/logos/re9000A.jpg
  - photo: /assets/logos/re9000B.jpg
  - photo: /assets/logos/re9000C.jpg
---
Rethinking Economics Gent (RE9000) ontstond in 2018 door studenten en onderzoekers vanuit verschillende achtergronden. 
Ze werden verenigd door teleurstelling in de nauwe blik die 'mainstream' economie heeft. 
Belangrijke onderwerpen, zoals klimaatverandering, armoede en ongelijkheid of machtsongelijkheid, komen daar weinig aan bod. 
Wanneer deze onderwerpen dan tóch aan bod komen, is het altijd met dezelfde, nauwe set van beperkte 'tools'. 
Resultaat: Economen hebben een vreemde(re) kijk op de wereld in vergelijking met hun collega sociale wetenschappers. 
De 'herdenkers' werden aangemoedigd door de internationale werking van de moederorganisatie 'Rethinking Economics' en voelden de nood om een lokaal verankerde werking op te zetten. 
De groep richt zich op iedereen die geïnteresseerd is in een economie die zich bezig houdt met relevante maatschappelijke problemen.
RE9000 draagt pluralisme en dus ook de betrokkenheid van andere wetenschappen hoog in het vaandel. 
Onze organisatie trekt studenten aan uit economische alsook niet-economische richtingen (bv. rechten, wijsbegeerte, geschiedenis als politieke wetenschappen). Alle kennisniveaus zijn welkom.
Via debatten, lezingen, acties, projecten, spelletjes en andere activiteiten op de campussen probeert Rethinking Economics publiek debat te stimuleren en zo veel mogelijk bij te leren over economie in relatie tot onze prachtige, complexe, maar ook kwetsbare wereld.
