---
titel: Home Vermeylen
id: vermeylen
naam: Home Vermeylen
verkorte_naam: Home Vermeylen
konvent: hk
contact: homeraadvermeylen@gmail.com
website: http://vermeylen.ugent.be/
social: 
  - platform: facebook
    link: https://www.facebook.com/Home-Vermeylen-845283782203005/
  - platform: instagram
    link: https://www.instagram.com/homevermeylen/
themas:
  -  home
showcase:
  - photo: /assets/logos/VermeylA.PNG
  - photo: /assets/logos/VermeylB.PNG
  - photo: /assets/logos/VermeylC.PNG
---

Home Vermeylen is eveneens aan de Overpoort gelegen - naast de Fabiola - en biedt onderdak aan maar liefst 456 inwoners! De acht verdiepingen tellende home beschikt verder over alle faciliteiten voor studenten met een functiebeperking.
In deze home bevindt zich ook de de permanentiedienst en de receptie waar je terecht kan met badge/sleutelproblemen. Dienst Huisvesting heeft hier verder ook haar kantoren.
De prachtige tuin van de home is de ideale plaats om bij mooi weer in te vertoeven. Als je toch binnen wil zitten, dan biedt de homeraad voldoende activiteiten aan. Af en toe vindt er een filmavond plaats, naast de wekelijkse ontspanningsavonden en fuiven.
Ook op cultureel vlak heeft de homeraad veel te bieden: voetbal op groot scherm, paintball en ons eigen festival zijn maar een greep uit het veelzijdige aanbod.
De home is genoemd naar August Vermeylen, hoogleraar, literair manusje van alles én medeoprichter van “Van nu en Straks”. Ook was “onzen” August de eerste rector van de vernederlandste Universiteit Gent (1930).
