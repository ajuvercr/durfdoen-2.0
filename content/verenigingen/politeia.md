---
titel: Politeia
id: politeia
naam: Politeia
verkorte_naam: Politeia
konvent: fk
website: http://www.politeia-gent.be/
contact: praeses@politeia-gent.be
social:
  - platform: instagram
    link: https://www.instagram.com/politeia_gent/
  - platform: youtube
    link: https://www.youtube.com/channel/UC51bn8-2xMtsDbBE6gxBRRQ
  - platform: facebook
    link: https://www.facebook.com/politeiaonline/
themas:
  -  faculteit
---
Politeia is dé studentenkring voor Pol en Soc’ers. Naast de klassieke activiteiten die studentenverenigingen organiseren – zoals cantussen en feestjes- staat Politeia garant voor een breed spectrum aan diverse culturele, sportieve en creatieve activiteiten. Gaande van het uittesten van nieuwe sporten, lezingen, de jaarlijkse skireis, praatcafés tot verhitte debatten. Iedereen vindt dus zonder twijfel zijn gading in dit ruime aanbod!
