---
titel: Zeus WPI
id: zeus
naam: Zeus WPI
verkorte_naam: Zeus WPI
themas:
  - wetenschap en techniek
konvent: wvk
contact: bestuur@zeus.ugent.be
website: https://zeus.ugent.be/
social:
  - platform: facebook
    link: https://www.facebook.com/zeus.wpi/
showcase:
  - photo: /assets/logos/ZeusA.jpg
  - photo: /assets/logos/ZeusB.jpg
  - photo: /assets/logos/ZeusC.jpg
---

Heb je je altijd al willen verdiepen in de duistere kunsten en willen leren hoe je computers kan laten doen wat je wil? Dan is Zeus WPI (of kort: Zeus) de vereniging voor jou! Zeus heeft haar lokaal in de kerkers (kelder) van S9, want informatici hebben toch geen zonlicht nodig...

Zeus WPI is dé studentenvereniging voor iedereen die geïntereseerd is in computers, informatica en alles daarrond.

Heb je (een idee voor) een project en wil je wat hulp, of feedback, wil je meewerken aan een reeds lopend (open-source) project, gewoon wat random dingen doen met computers of een plekje hebben om samen te zitten met andere informatici? Dan is de kelder "the place to be". Nog geen ervaring met programeren of grotere projecten? Dan helpen wij je op weg!

Meerdere keren per academiejaar organiseren we code-nights! Dit zijn avonden waar het vooral gaat om het gezellig samen zijn, maar natuurlijk wordt er nog geprogrammeerd ook, want in ons hart blijven we altijd informatici.

Deze code-nights staan soms in het thema van één van onze talloze projecten; het telsysteem voor de 12-urenloop, de Hydra app (zeker eens proberen), en gamification (want voor ons is programeren soms een wedstrijd, maar deelnemen is belangrijker dan winnen).

Tijdens de week ben je ook altijd welkom om je passie voor informatica te delen of gewoon even gezellig samen te zitten en wat ideeën uit te wisselen.

Ook voor een Linux-install (er is ook de Linux Install Party, een event waar we Linux installeren) kan je altijd bij ons terecht!

Voor een lijstje van projecten waar we mee bezig zijn, zie onze [GitHub](https://github.com/ZeusWPI).
