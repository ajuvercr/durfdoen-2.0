---
titel: "'t Wielke"
id: wielke
naam: "'t Wielke"
verkorte_naam: "'t Wielke"
konvent: sk
website: http://www.wielke.be
social:
  - platform: instagram
    link: https://www.instagram.com/hsctwielke/
  - platform: facebook
    link: https://www.facebook.com/tWielke/
themas:
  - streek
postcodes:
  - 8790
  - 8791
  - 8792
  - 8793
  - 8930
---

HSC ’t Wielke is een West-Vlaamse studentenclub die sinds 2008 het Gentse studentenleven weer opfleurt voor menig student. Het verzamelt studenten uit de regio Menen-Waregem.
Tussen het studeren door zorgt ‘t Wielke voor de nodige ontspanning in Gent én Menen. Een greep uit onze evenementen: cultuur- en sportactiviteiten, de traditionele cantussen, het Wielkesweekend, talloze feestjes en een niet te missen spetterend Galabal! Naar goede traditie zijn we ook jaarlijks aanwezig op de Gravensteenfeesten en de massacantus van Gent! Iedere maandag zijn we te vinden in ons clubcafé, De Coulissen te Gent. Ook de rest van de week kan je er zeker en vast Wielkesleden tegen het lijf lopen. Onze cantussen vinden plaats in de Salamander, het café van onze marraine Rita.
