---
naam: StuBio
verkorte_naam: StuBio
titel: Faculteitsraad Bio-ingenieurswetenschappen
id: stubio
naam: Faculteitsraad Bio-ingenieurswetenschappen
konvent: gsr
website: https://stubio.ugent.be
contact: stubio@ugent.be
social:
  - platform: facebook
    link: https://www.facebook.com/stubio.gent/
themas:
  - engagement
---
StuBio is de facultaire studentenraad bio-ingenieurswetenschappen. Als studentenvertegenwoordigers verdedigen wij de studenten hun belangen op allerlei raden en commissies aan de faculteit. Ons doel is om elke FBW student, ongeacht of die les volgt op campus Kortrijk, Schoonmeersen, Coupure of Korea, te vertegenwoordigen en hun mening hoorbaar maken. Dit kan gaan over fietsenstallingen, nieuwe gebouwen, thesis deadlines,....
Wij vertegenwoordigen alle studenten in de verscheidene commissies & raden, nemen deel aan de vergaderingen van de Gentse Studentenraad (GSR), duiden de ombudspersonen aan, keuren de examenroosters goed, ...
Wil je even jouw mening kwijt? Kom gerust naar onze maandelijkse AV's, deze zijn open voor alle studenten en gaan steeds over zeer relevante topics voor de studenten.
