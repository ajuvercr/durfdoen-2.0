---
titel: Comac
id: comac
naam: Comac
verkorte_naam: Comac
konvent: pfk
website: https://www.comac-studenten.be/
social:
  - platform: facebook
    link: https://www.facebook.com/comacstudenten/
  - platform: instagram
    link: https://www.instagram.com/comac_studenten/
themas:
  - politiek en filosofisch
showcase:
  - photo: /assets/logos/ComacA.jpg
  - photo: /assets/logos/ComacB.jpg
  - photo: /assets/logos/ComacC.jpg
---

Comac is de nationale studentenbeweging van de PVDA, dat wil zeggen dat we zowel aan Vlaamse als Waalse universiteiten actief zijn.
Comac zet zich in voor sociale rechtvaardigheid. Wij gaan voor een heel andere maatschappij: eentje zonder armoede en uitbuiting. We geloven in een samenleving waar racisme, seksisme en eender welke andere vorm van discriminatie geen plaats hebben; in een wereld waar elke jongere toegang heeft tot kwaliteitsvol onderwijs; in een economie die niet in functie staat van winstbejag, maar in functie van het welzijn van mens en natuur.
Comac brengt jongeren samen die willen bouwen aan een sociale, duurzame en democratische toekomst.
In Gent hebben we al vele geslaagde acties achter de rug: vele lezingen, van het redden van het klimaat tot verzet tegen extreem-rechts, aandacht voor mentale gezondheid tot solidariteit met Palestina maar ook acties om de samenwerking tussen UGent en Monsanto aan te klagen of terugkerende activiteiten zoals onze collectieve blok waar we op verplaatsing blokken om samen te slagen.
We komen ook wekelijks samen in de basisgroep waar we de politieke actualiteit bestuderen en acties plannen in en rond de campus.
Wil je graag de wereld veranderen en denk je dat comac wel iets voor jou is? Heb je nog vragen? Neem gerust contact op via de website, facebook of Instagram!
