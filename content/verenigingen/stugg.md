---
naam: StuGG
verkorte_naam: StuGG
titel: Studentenraad Geneeskunde en Gezondheidswetenschappen
id: stugg
naam: Studentenraad Geneeskunde en Gezondheidswetenschappen
konvent: gsr
website: https://www.stugg.be
contact: info@stugg.be
social:
  - platform: facebook
    link: https://www.facebook.com/stugggent/
themas:
  -  engagement
---

StuGG is de overkoepelende studentenraad aan de Faculteit Geneeskunde en Gezondheidswetenschappen. Aan onze Faculteit wordt iedere opleiding vertegenwoordigd door een studentenraad, die zich vooral bezighoudt met verbeteringen en problemen die zich binnen de opleiding voordoen. Zo hebben studenten onder andere inspraak in de indeling van examenroosters, hervorming van curricula en evaluatie van de opleidingsonderdelen. StuGG overziet de werking van de opleidingsgebonden studentenraden en ondersteunt hen waar nodig, maar houdt zich ook bezig met bredere thema’s die iedere student aangaan. Dit kan gaan om kwaliteitsvol onderwijs in het algemeen (bijv. lesopnames, onderwijsevaluaties), alles aangaande infrastructuur (bijv. bibliotheek) en universiteitsbrede thema’s, maatregelen en/of campagnes (bijv. duurzaamheid, diversiteit). Wil jij ook wel het een en ander kwijt over een van deze thema’s? Of wil je meer info over onze werking? Kom dan zeker eens een kijkje nemen op een van onze vergaderingen.Tot dan!
