---
titel: Scaldis
id: scaldis
naam: Scaldis
verkorte_naam: Scaldis
konvent: sk
website: https://scaldis.skghendt.be/
social:
  - platform: facebook
themas:
  - streek
postcodes:
  - 9200
  - 9220
  - 9240
  - 9255
  - 9260
  - 9280
  - 9290
---

De enige studentenclub van studenten uit Dendermonde die studeren in Gent. Bij ons kan je terecht voor activiteiten zoals de replica van het Ros Beiaard, cocktailavonden en gyros met rijst-avonden.
