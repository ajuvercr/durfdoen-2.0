---
naam: StuArt
verkorte_naam: StuArt
titel: Studentenraad Faculteit Letteren en Wijsbegeerte
id: stuart
naam: Studentenraad Faculteit Letteren en Wijsbegeerte
konvent: gsr
website: https://stuart.ugent.be
contact: stuart@ugent.be
social:
  - platform: facebook
    link: https://www.facebook.com/StudentenraadStuArt/
  - platform: instagram
    link: https://www.instagram.com/stuart.studentenraad/
themas:
  -  engagement
---

StuArt is de afkorting voor studenten artes en vertegenwoordigt de studentenraad van de faculteit Letteren en Wijsbegeerte. StuArt is een collectief van zowel verkozen studentenvertegenwoordigers (stuvers) als vrijwillige, geëngageerde studenten die zich bezighouden met actuele thema’s en debatten aan de faculteit. Als studentenvertegenwoordigers verdedigen we de belangen van alle studenten, ongeacht de richting of het studiejaar, door in verschillende raden en commissies te zetelen. Op die vergaderingen vertegenwoordigen we de stem van de studenten en proberen we de faculteit een betere plek te maken voor alle betrokken partijen. Naast de vaste vergaderingen staan we ook altijd klaar om met jullie vragen, ideeën en suggesties aan de slag te gaan. We kaarten jullie opmerkingen aan op de vergaderingen en pleiten voor een oplossing, of we verwijzen jullie door naar de juiste instanties voor snelle hulp. Op die manier bepalen jullie als studenten dus zelf welke punten aangepakt worden en bepalen jullie mee de agenda.
