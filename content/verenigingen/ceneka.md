---
titel: CenEka
id: ceneka
naam: CenEka
verkorte_naam: CenEka
konvent: wvk
website: https://www.ceneka.be
contact: info@ceneka.be
social:
  - platform: facebook
    link: https://www.facebook.com/CenEkaGent/
  - platform: instagram
    link: https://www.instagram.com/cenekagent/
  - platform: linkedin
    link: https://www.linkedin.com/company/ceneka/
themas:
  -  wetenschap en techniek
---

De naam CenEka past binnen het rijtje van alle grote (digitale) platformen aan de UGent die hun inspiratie halen in de klassieke oudheid. Enkel de spelling is twee millennia later wat anders: de C staat voor de Computerwetenschappen en de E voor Elektrotechniek. Ook wij willen een platform zijn voor studenten burgerlijk ingenieur met een interesse voor elektronica, ICT, en alle digitale trends. Dit doen we met verschillende lezingen, bedrijfsbezoeken en workshops, alsook informele activiteiten om het groepsgevoel te versterken.
Met onze evenementen ondersteunen we de professionele ontwikkeling van toekomstige ingenieurs, bieden we hen een blik binnen in de bedrijfswereld, en bouwen we samen een professioneel netwerk op. Omdat we iedereen met even open armen te ontvangen, vraagt CenEka geen lidgeld. Iedere student met interesse kan vrij deelnemen en geniet van dezelfde voordelen.
Op ons grootse evenement, de EMECS Career & Technology beurs, brengen we je in contact met meer dan 30 bedrijven en worden er doorlopend lezingen gegeven over de allernieuwste ontwikkelingen bij Vlaamse hightech bedrijven.
