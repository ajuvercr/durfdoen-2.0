---
titel: GUDc
id: gudc
naam: GUDc
verkorte_naam: GUDc
konvent: kultk
contact: info@gudc.be
website: http://gudc.be/
social:
  - platform: facebook
    link: https://www.facebook.com/GentseUniversitaireDansclub/
themas:
  -  cultuur
showcase:
  - photo: /assets/logos/GUDcA.jpg
  - photo: /assets/logos/GUDcB.png
  - photo: /assets/logos/GUDcC.jpg
---

Heb je zin om je eens goed uit te leven? Trek dan alvast je dansschoenen aan, want bij de Gentse Universitaire Dansclub (GUDc) ben je aan het juiste adres! De GUDc is de allereerste en enige universitaire dansvereniging in België. De club is opgestart in 2007 om de danssport onder de studenten te promoten. Onze hoofd-activiteit is het aanbieden van ballroom- en latindansen . Hieronder vinden we o.a. de Engelse wals, quickstep, jive,tango,… Naast onze wekelijkse lessen ballroom en latin organiseren we ook tal van activiteiten waaronder dans-workshops. Dit kan gaan van hiphop tot oriëntaalse dans , naargelang er vraag naar is. Verder organiseren we ook een filmavond, quiz en oefenavonden, steeds met ‘dans’ als centraal thema. Naar de toekomst toe zal de vereniging een groter aanbod van dansstijlen aanbieden, en deelnemen aan (inter)nationale wedstrijden.
