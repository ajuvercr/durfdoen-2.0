---
titel: FLYSE
id: flyse
naam: FLYSE
verkorte_naam: FLYSE
konvent: wvk
contact: info@flyse.be
website: https://www.flyse.be
social:
  - platform: instagram
    link: https://www.instagram.com/flysegent/
  - platform: facebook
    link: https://www.facebook.com/FLYSE/
  - platform: linkedin
    link: https://www.linkedin.com/company/flyse/
  - platform: twitter
    link: https://twitter.com/FLYSE_BE
themas:
  - economie en development
showcase:
  - photo: /assets/logos/flyseA.jpg
  - photo: /assets/logos/flyseB.jpg
  - photo: /assets/logos/flyseC.jpg
---

De Flanders Youth Society for Entrepreneurship, kortweg FLYSE, inspireert reeds 9 jaar tot ondernemerschap in het Gentse studentenmilieu. Het FLYSE-bestuur en alle FLYSE-leden zijn stuk voor stuk jonge, gemotiveerde studenten die ernaar streven om ondernemerschap te promoten.
