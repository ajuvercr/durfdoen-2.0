---
titel: AIESEC
id: aiesec
naam: AIESEC
verkorte_naam: AIESEC
konvent: ik
contact: membership.gent@aiesec.be
website: https://aiesec.org/
social:
  - platform: instagram
  - platform: facebook
    link: https://www.facebook.com/AIESECinBelgium/
themas:
  - economie en development
  - internationaal
---

AIESEC is the biggest youth organization worldwide. We provide opportunities for young people to develop their leadership skills through cross-cultural exchanges. Our offers in exchange include paid and volunteer internships.
