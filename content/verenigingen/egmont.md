---
titel: Moeder Egmont
id: egmont
naam: Moeder Egmont
verkorte_naam: Moeder Egmont
konvent: sk
website: https://www.moederegmont.be
contact: studentenclubmoederegmont@gmail.com
social:
  - platform: facebook
themas:
  - streek
postcodes:
  - 9520
  - 9521
  - 9550
  - 9551
  - 9552
  - 9620
  - 9630
  - 9636
  - 9660
  - 9661
  - 9860
---

Wij zijn de sociale en sportieve studentenclub voor Gentse studenten uit Zottegem. Het hoogtepunt van het jaar is steeds het Bal van Moeder Egmont. In de week hebben we tal van activiteiten in Gent, in het weekend in Zottegem zelf. De clubavonden in Gent gaan door op woensdag in café Kraaienest, die in Zottegem in café James, elke eerste zaterdag van de maand.
