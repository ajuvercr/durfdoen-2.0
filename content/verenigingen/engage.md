---
titel: ENGAGE
id: engage
naam: ENGAGE
verkorte_naam: ENGAGE
konvent: wvk
contact: engagefchange@gmail.com 
website: http://engage4change.be/
social:
  - platform: facebook
    link: https://www.facebook.com/engageugent/
  - platform: instagram
    link: https://www.instagram.com/engage.ugent/
themas:
  -  politiek en filosofisch
  -  lifestyle
  - engagement
showcase:
  - photo: /assets/logos/EngageA.jpg
  - photo: /assets/logos/EngageB.jpg
  - photo: /assets/logos/EngageC.jpg
---

Engage wilt een alternatief te bieden voor klassieke studentenkringen aan studenten die net dat ietsje meer zoeken naast hun studies binnen hun specifieke opleiding. Het is ons doel studenten te helpen in de vorming van een kritische gecultiveerde geest, dat breder denkt dan enkel binnen zijn of haar vakgebied. We willen studenten warm maken actief bezig te zijn met wat er in de samenleving speelt. We willen studenten stimuleren om daar zelf op een positieve manier aan bij te dragen. Ook willen we een brug slaan tussen verschillende groepen in de samenleving. Engage is een platform dat ruimte creëert om elkaar beter te leren kennen. Ruimte voor open debat en dialoog in een cultuur van wederzijds respect en open-mindedness. Dit doen we onder andere door middel van het organiseren van studentenvormingen, workshops, debatten (onze engage Trials), dialoogtafels en lezingen. Dit over allerhande thema’s die aansluiten bij onze doelstellingen. Maar daarnaast is er ook voldoende ruimte gezellige game- quiz- en film-avonden alsook open mic events.
