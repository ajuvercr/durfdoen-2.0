---
titel: CDS
id: cds
naam: CDS
verkorte_naam: CDS
konvent: pfk
website: http://www.cdsnationaal.be/
social:
  - platform: facebook
    link: https://www.facebook.com/cds.gent/
themas:
  - politiek en filosofisch
showcase:
  - photo: /assets/logos/CDSA.JPG
  - photo: /assets/logos/CDSB.jpg
  - photo: /assets/logos/CDSC.jpg
---

CDS Gent is een politieke studentenvereniging aan de UGent, gegrondvest op de principes van de christendemocratie en het personalisme.
Onze missie is studenten warm te maken voor politiek in het algemeen en de christendemocratie in het bijzonder.
Dit doen we door tal van activiteiten te organiseren gaande van lezingen, debatten, discussieavonden tot de meer studentikoze activiteiten.
We zijn een officieel erkende politieke studentenvereniging en maken dus deel uit van het Gentse Politiek Filosofisch Konvent (PFK), de koepel van alle politiek filosofische studentenverenigingen.
Verder maakt CDS Gent, samen met de andere kernen in Antwerpen en Leuven, deel uit van CDS Nationaal.
Op Europees niveau is CDS volwaardig lid van de European Democrat Students, de studentenvereniging van de Europese Volkspartij.
