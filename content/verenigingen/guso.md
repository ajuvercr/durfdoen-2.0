---
titel: GUSO
id: guso
naam: GUSO
verkorte_naam: GUSO
konvent: kultk
contact: info@guso.be
website: http://guso.be/
social:
  - platform: facebook
    link: https://www.facebook.com/GentsUniversitairSymfonischOrkest/
  - platform: instagram
    link: https://www.instagram.com/g.u.s.o/
themas:
  -  cultuur
showcase:
  - photo: /assets/logos/GUSOA.jpg
  - photo: /assets/logos/GUSOB.jpg
  - photo: /assets/logos/GUSOC.jpg
---

Het Gents Universitair Symfonisch Orkest (GUSO) is een bruisend symfonisch studentenorkest, helemaal door én voor studenten. Behalve samen musiceren, staat GUSO garant voor een aangename sfeer, een hoop vrienden, motiverende stages met naast repetities nog een resem andere activiteiten,tijdens het semester ook nog pizza-dates, zwemavonden, een bowling challenge... en als bekroning een sprankelend dubbelconcert! De wekelijkse repetities gaan door in de Therminal op woensdag van 19u30 tot 22u, en elk semester vertrekken we op stage. De herfststage vindt plaats binnen België, de voorjaarsstage is traditioneel een buitenlandse stage. Zo ging GUSO al naar de University of Cambridge en was er een uitwisseling met het Jeugdorkest van Valladolid. Drie jaar geleden ging GUSO naar Berlijn, we proefden ook al van Amsterdam en in april dit jaar repeteerden en concerteerden we in een authentieke rurale Franse omgeving.
